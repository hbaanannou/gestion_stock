package com.manwa.stock.mvc.dao;

import com.manwa.stock.mvc.entities.Article;

public interface IArticleDao extends IGenericDao<Article>{

}
