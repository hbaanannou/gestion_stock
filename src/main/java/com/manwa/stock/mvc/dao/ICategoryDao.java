package com.manwa.stock.mvc.dao;

import com.manwa.stock.mvc.entities.Category;

public interface ICategoryDao extends IGenericDao<Category>{

}
