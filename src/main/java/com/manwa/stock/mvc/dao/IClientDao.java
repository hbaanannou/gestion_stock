package com.manwa.stock.mvc.dao;


import com.manwa.stock.mvc.entities.Client;

public interface IClientDao extends IGenericDao<Client>{

}
