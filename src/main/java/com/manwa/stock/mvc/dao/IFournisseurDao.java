package com.manwa.stock.mvc.dao;

import com.manwa.stock.mvc.entities.Fournisseur;

public interface IFournisseurDao extends IGenericDao<Fournisseur>{

}
