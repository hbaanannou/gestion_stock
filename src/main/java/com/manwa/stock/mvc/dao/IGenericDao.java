package com.manwa.stock.mvc.dao;

import java.util.List;

public interface IGenericDao<E> {
	
	public E save(E entity);
	public E update(E entity);
	public List<E> selectAll();
	public E getById(Long id);
	public void remove(Long id);
	public List<E>  selectAll(String sortFiled, String sort);
	public E findOneBy(String paramName,Object paramValue);
    public E findOneBy(String[] paramNames, Object[] paramValues );
    public int findCountBy(String paramName,String paramValue);
}
