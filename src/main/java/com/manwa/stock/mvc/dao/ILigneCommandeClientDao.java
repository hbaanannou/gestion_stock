package com.manwa.stock.mvc.dao;

import java.util.List;

import com.manwa.stock.mvc.entities.LigneCommandeClient;

public interface ILigneCommandeClientDao extends IGenericDao<LigneCommandeClient> {
	public List<LigneCommandeClient> getByIdCommande(Long idCommandeClient);
}
