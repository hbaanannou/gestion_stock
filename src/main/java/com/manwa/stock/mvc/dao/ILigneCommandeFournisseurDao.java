package com.manwa.stock.mvc.dao;

import com.manwa.stock.mvc.entities.LigneCommandeFournisseur;

public interface ILigneCommandeFournisseurDao extends IGenericDao<LigneCommandeFournisseur> {

}
