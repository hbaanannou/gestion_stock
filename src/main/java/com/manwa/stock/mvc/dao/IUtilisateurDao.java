package com.manwa.stock.mvc.dao;

import com.manwa.stock.mvc.entities.Utilisateur;

public interface IUtilisateurDao extends IGenericDao<Utilisateur>{

}
