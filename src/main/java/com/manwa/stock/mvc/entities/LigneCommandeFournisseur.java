package com.manwa.stock.mvc.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@SuppressWarnings("serial")
@Entity
public class LigneCommandeFournisseur implements Serializable{
	/**
	 * 
	 */
	


	/**
	 * 
	 */
	


	@Id
	@GeneratedValue
	private Long id;

	
	@ManyToOne
	@JoinColumn(name = "idCommandeFouurnisseur")
	private CommandeFournisseur commandeFournisseur;

	@ManyToOne
	@JoinColumn(name = "idArticle")
	private Article article;
	
	
	
	public CommandeFournisseur getCommandeFournisseur() {
		return commandeFournisseur;
	}

	public void setCommandeFournisseur(CommandeFournisseur commandeFournisseur) {
		this.commandeFournisseur = commandeFournisseur;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long idLigneCommandeFournisseur) {
		this.id = idLigneCommandeFournisseur;
	}
	
	
}
