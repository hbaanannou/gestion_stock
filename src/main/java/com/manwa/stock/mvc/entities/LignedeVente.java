package com.manwa.stock.mvc.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@SuppressWarnings("serial")
@Entity
public class LignedeVente implements Serializable {
	/**
	 * 
	 */
	

	@Id
	@GeneratedValue
	private Long idLignedeVente;

	 @ManyToOne
	 @JoinColumn(name = "idVente")
	 private Vente vente;

	@ManyToOne
	@JoinColumn(name = "idArticle")
	private Article article;

	public Long getIdLignedeVente() {
		return idLignedeVente;
	}

	public void setIdLignedeVente(Long idLignedeVente) {
		this.idLignedeVente = idLignedeVente;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public Long getId() {
		return idLignedeVente;
	}

	public void setId(Long id) {
		this.idLignedeVente = id;
	}

	public Vente getVente() {
		return vente;
	}

	public void setVente(Vente vente) {
		this.vente = vente;
	}
	
	
}

