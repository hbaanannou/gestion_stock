package com.manwa.stock.mvc.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@SuppressWarnings("serial")
@Entity
public class Vente implements Serializable {
	/**
	 * 
	 */
	

	@Id
	@GeneratedValue
	private Long idVente;
	
	private String code;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateVente;

	@OneToMany(mappedBy="vente")
	private List<LignedeVente> lignedeVentes;
	
	
	public Long getIdVente() {
		return idVente;
	}

	public void setIdVente(Long idVente) {
		this.idVente = idVente;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getDateVente() {
		return dateVente;
	}

	public void setDateVente(Date dateVente) {
		this.dateVente = dateVente;
	}

	public List<LignedeVente> getLignedeVentes() {
		return lignedeVentes;
	}

	public void setLignedeVentes(List<LignedeVente> lignedeVentes) {
		this.lignedeVentes = lignedeVentes;
	}

	public Long getId() {
		return idVente;
	}

	public void setId(Long id) {
		this.idVente = id;
	}
	
	
}
