package com.manwa.stock.mvc.services;

import java.util.List;

import com.manwa.stock.mvc.entities.Category;


public interface ICategoryService {
	public Category save(Category entity);
	public Category update(Category entity);
	public List<Category> selectAll();
	public Category getById(Long id);
	public void remove(Long id);
	public List<Category>  selectAll(String sortFiled, String sort);
	public Category findOneBy(String paramName,Object paramValue);
    public Category findOneBy(String[] paramNames, Object[] paramValues );
    public int findCountBy(String paramName,String paramValue);
}
