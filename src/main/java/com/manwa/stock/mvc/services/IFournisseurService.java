package com.manwa.stock.mvc.services;

import java.util.List;

import com.manwa.stock.mvc.entities.Fournisseur;

public interface IFournisseurService {
	
	public Fournisseur save(Fournisseur entity);
	
	public Fournisseur update(Fournisseur entity);

	public List<Fournisseur> selectAll();

	public List<Fournisseur> selectAll(String sortField, String sort);

	public Fournisseur getById(Long id);

	public void remove(Long id);

	public Fournisseur findOneBy(String paramName, Object paramValue);

	public Fournisseur findOneBy(String[] paramNames, Object[] paramValues);

	public int findCountBy(String paramName, String paramValue);

}
