package com.manwa.stock.mvc.services;

import java.util.List;

import com.manwa.stock.mvc.entities.LignedeVente;

public interface ILigneVenteService {
	
	public LignedeVente save(LignedeVente entity);
	
	public LignedeVente update(LignedeVente entity);

	public List<LignedeVente> selectAll();

	public List<LignedeVente> selectAll(String sortField, String sort);

	public LignedeVente getById(Long id);

	public void remove(Long id);

	public LignedeVente findOneBy(String paramName, Object paramValue);

	public LignedeVente findOneBy(String[] paramNames, Object[] paramValues);

	public int findCountBy(String paramName, String paramValue);

}
