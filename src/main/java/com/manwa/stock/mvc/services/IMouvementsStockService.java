package com.manwa.stock.mvc.services;

import java.util.List;

import com.manwa.stock.mvc.entities.MouvementsStock;;

public interface IMouvementsStockService {
	
	public MouvementsStock save(MouvementsStock entity);
	
	public MouvementsStock update(MouvementsStock entity);

	public List<MouvementsStock> selectAll();

	public List<MouvementsStock> selectAll(String sortField, String sort);

	public MouvementsStock getById(Long id);

	public void remove(Long id);

	public MouvementsStock findOneBy(String paramName, Object paramValue);

	public MouvementsStock findOneBy(String[] paramNames, Object[] paramValues);

	public int findCountBy(String paramName, String paramValue);

}
