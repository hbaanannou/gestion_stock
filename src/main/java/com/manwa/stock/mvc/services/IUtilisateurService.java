package com.manwa.stock.mvc.services;

import java.util.List;

import com.manwa.stock.mvc.entities.Utilisateur;

public interface IUtilisateurService {
	
	public Utilisateur save(Utilisateur entity);
	
	public Utilisateur update(Utilisateur entity);

	public List<Utilisateur> selectAll();

	public List<Utilisateur> selectAll(String sortField, String sort);

	public Utilisateur getById(Long id);

	public void remove(Long id);

	public Utilisateur findOneBy(String paramName, Object paramValue);

	public Utilisateur findOneBy(String[] paramNames, Object[] paramValues);

	public int findCountBy(String paramName, String paramValue);

}
