package com.manwa.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.manwa.stock.mvc.dao.IArticleDao;
import com.manwa.stock.mvc.entities.Article;
import com.manwa.stock.mvc.services.IArticleService;

@Transactional
public class ArticleServiceImpl implements IArticleService{

	private IArticleDao dao;
	
	public void setDao(IArticleDao dao) {
		this.dao = dao;
	}
	
	@Override
	public Article save(Article entity) {
		
		return dao.save(entity);
	}

	@Override
	public Article update(Article entity) {
		
		return dao.update(entity);
	}

	@Override
	public List<Article> selectAll() {
		
		return dao.selectAll();
	}

	@Override
	public Article getById(Long id) {
		
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
		
	}

	@Override
	public List<Article> selectAll(String sortFiled, String sort) {
		
		return dao.selectAll(sortFiled, sort);
	}

	@Override
	public Article findOneBy(String paramName, Object paramValue) {
		
		return dao.findOneBy(paramName, (String) paramValue);
	}

	@Override
	public Article findOneBy(String[] paramNames, Object[] paramValues) {
		
		return dao.findOneBy(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		
		return dao.findCountBy(paramName, paramValue);
	}

}
