package com.manwa.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.manwa.stock.mvc.dao.ICategoryDao;
import com.manwa.stock.mvc.entities.Category;
import com.manwa.stock.mvc.services.ICategoryService;

@Transactional
public class CategoryServiceImpl implements ICategoryService{

	private ICategoryDao dao;
	
	public void setDao(ICategoryDao dao) {
		this.dao = dao;
	}
	
	@Override
	public Category save(Category entity) {
		
		return dao.save(entity);
	}

	@Override
	public Category update(Category entity) {
		
		return dao.update(entity);
	}

	@Override
	public List<Category> selectAll() {
		
		return dao.selectAll();
	}

	@Override
	public Category getById(Long id) {
		
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
		
	}

	@Override
	public List<Category> selectAll(String sortFiled, String sort) {
		
		return dao.selectAll(sortFiled, sort);
	}

	@Override
	public Category findOneBy(String paramName, Object paramValue) {
		
		return dao.findOneBy(paramName, paramValue);
	}

	@Override
	public Category findOneBy(String[] paramNames, Object[] paramValues) {
		
		return dao.findOneBy(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		
		return dao.findCountBy(paramName, paramValue);
	}

}
