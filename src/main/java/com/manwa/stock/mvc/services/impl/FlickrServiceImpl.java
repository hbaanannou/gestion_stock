package com.manwa.stock.mvc.services.impl;

import java.io.InputStream;

import org.springframework.transaction.annotation.Transactional;

import com.manwa.stock.mvc.dao.IFlickrDao;
import com.manwa.stock.mvc.services.IFlickrService;

@Transactional
public class FlickrServiceImpl implements IFlickrService {
	private IFlickrDao dao;
	@Override
	public String savePhoto(InputStream photo, String title) throws Exception {
		
		return dao.savePhoto(photo, title);
	}
	public IFlickrDao getDao() {
		return dao;
	}
	public void setDao(IFlickrDao dao) {
		this.dao = dao;
	}

}
