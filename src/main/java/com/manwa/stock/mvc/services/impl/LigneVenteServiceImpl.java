package com.manwa.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.manwa.stock.mvc.dao.ILignedeVenteDao;
import com.manwa.stock.mvc.entities.LignedeVente;
import com.manwa.stock.mvc.services.ILigneVenteService;

@Transactional
public class LigneVenteServiceImpl implements ILigneVenteService {
	
	private ILignedeVenteDao dao;
	
	public void setDao(ILignedeVenteDao dao) {
		this.dao = dao;
	}

	@Override
	public LignedeVente save(LignedeVente entity) {
		return dao.save(entity);
	}

	@Override
	public LignedeVente update(LignedeVente entity) {
		return dao.update(entity);
	}

	@Override
	public List<LignedeVente> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<LignedeVente> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public LignedeVente getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	@Override
	public LignedeVente findOneBy(String paramName, Object paramValue) {
		return dao.findOneBy(paramName, paramValue);
	}

	@Override
	public LignedeVente findOneBy(String[] paramNames, Object[] paramValues) {
		return dao.findOneBy(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}

}
