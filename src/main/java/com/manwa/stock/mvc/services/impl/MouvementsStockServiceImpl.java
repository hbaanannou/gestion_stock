package com.manwa.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.manwa.stock.mvc.dao.IMouvementsStockDao;
import com.manwa.stock.mvc.entities.MouvementsStock;
import com.manwa.stock.mvc.services.IMouvementsStockService;

@Transactional
public class MouvementsStockServiceImpl implements IMouvementsStockService {
	
	private IMouvementsStockDao dao;
	
	public void setDao(IMouvementsStockDao dao) {
		this.dao = dao;
	}

	@Override
	public MouvementsStock save(MouvementsStock entity) {
		return dao.save(entity);
	}

	@Override
	public MouvementsStock update(MouvementsStock entity) {
		return dao.update(entity);
	}

	@Override
	public List<MouvementsStock> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<MouvementsStock> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public MouvementsStock getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	@Override
	public MouvementsStock findOneBy(String paramName, Object paramValue) {
		return dao.findOneBy(paramName, paramValue);
	}

	@Override
	public MouvementsStock findOneBy(String[] paramNames, Object[] paramValues) {
		return dao.findOneBy(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}

}
